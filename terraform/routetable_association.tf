# Associating route tables and subnets
resource "aws_route_table_association" "rt_ass_lb" {
  count     = length(var.cidr_block_loadbalancer)
  subnet_id = element(aws_subnet.loadbalancer.*.id, count.index)
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "rt_ass_app" {
  subnet_id      = aws_subnet.backend.id
  route_table_id = aws_route_table.private_rt.id
}
