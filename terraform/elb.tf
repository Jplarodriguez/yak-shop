# This load balancer is not logging anywhere for now

resource "aws_lb" "yakshop_lb" {
  name               = "yakshop-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb_sg.id]
  subnets            = aws_subnet.loadbalancer.*.id

  enable_deletion_protection = true

  tags = {
    Name = "yakshop_lb"
  }
}

resource "aws_lb_target_group" "yakshop" {
  name        = "yakshop-tg-lb"
  port        = 8080
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.yakshop_vpc.id

  health_check {
    port     = 8080
    protocol = "HTTP"
    timeout  = 5
    interval = 10
  }
}

resource "aws_lb_listener" "yakshop_lb_http" {
  load_balancer_arn = aws_lb.yakshop_lb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.yakshop.arn
  }
}


