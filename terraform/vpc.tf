resource "aws_vpc" "yakshop_vpc" {
  cidr_block       = var.cidr_block_vpc
  instance_tenancy = "default"

  tags = {
    Name = "yakshop_vpc"
  }
}

# Own yakshop backend
resource "aws_subnet" "backend" {
  vpc_id     = aws_vpc.yakshop_vpc.id
  cidr_block = var.cidr_block_backend

  tags = {
    Name = "yakshop_backend"
  }
}

# Using AWS LB 
resource "aws_subnet" "loadbalancer" {
  vpc_id            = aws_vpc.yakshop_vpc.id
  count             = length(var.availability_zone)
  cidr_block        = element(var.cidr_block_loadbalancer, count.index)
  availability_zone = element(var.availability_zone, count.index)

  tags = {
    Name = "yakshop_lb_${count.index}"
  }
}

# IGW from LB to Internet
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.yakshop_vpc.id

  tags = {
    Name = "yakshop_igw"
  }
}
