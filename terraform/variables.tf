variable "availability_zone" {
  type    = list(string)
  default = ["eu-west-3a", "eu-west-3b"]
}

variable "cidr_block_loadbalancer" {
  type    = list(string)
  default = ["10.0.0.0/24", "10.0.1.0/24"]
}

variable "cidr_block_vpc" {
  type    = string
  default = "10.0.0.0/16"
}

variable "cidr_block_backend" {
  type    = string
  default = "10.0.2.0/24"
}
