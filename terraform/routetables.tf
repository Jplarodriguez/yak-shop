# APP subnet
resource "aws_route_table" "private_rt" {
  vpc_id = aws_vpc.yakshop_vpc.id

  route = [
    {
      cidr_block = "10.0.2.0/24"
    }
  ]

  tags = {
    Name = "private_rt"
  }
}

# API/LB subnet
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.yakshop_vpc.id

  route = [
    {
      cidr_block = "10.0.0.0/24"
    }
  ]

  tags = {
    Name = "public_rt"
  }
}