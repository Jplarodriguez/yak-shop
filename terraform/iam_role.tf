resource "aws_iam_role" "yakshop_role" {
  name = "yakshop_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_policy" "ecs_policy" {
  name = "ecs_policy"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
          "ec2:Describe*",
          "elasticloadbalancing:*",
        ]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_policy_attachment" "ecs_role_attachment" {
  name       = "ecs_role_attachment"
  roles      = [aws_iam_role.yakshop_role.name]
  policy_arn = aws_iam_policy.ecs_policy.arn
}
