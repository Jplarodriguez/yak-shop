# NACL groups

resource "aws_network_acl" "nacls_lb" {
  vpc_id     = aws_vpc.yakshop_vpc.id
  subnet_ids = aws_subnet.*.id
  #subnet_ids = [aws_subnet.loadbalancer[0].id, aws_subnet.loadbalancer[1].id]

  tags = {
    Name = "nacls_lb"
  }
}

resource "aws_network_acl" "nacls_api" {
  vpc_id     = aws_vpc.yakshop_vpc.id
  subnet_ids = [aws_subnet.backend.id]

  tags = {
    Name = "nacls_api"
  }
}

# NACLS rules

# LB/API group

## Ingress rules

resource "aws_network_acl_rule" "nacls_lb1" {
  network_acl_id = aws_network_acl.nacls_lb.id
  rule_number    = 100
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 80
  to_port        = 80
}

resource "aws_network_acl_rule" "nacls_lb2" {
  network_acl_id = aws_network_acl.nacls_lb.id
  rule_number    = 110
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 443
  to_port        = 443
}

resource "aws_network_acl_rule" "nacls_lb3" {
  network_acl_id = aws_network_acl.nacls_lb.id
  rule_number    = 120
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.0.0/16"  # Allow all within VPC
  from_port      = 1024
  to_port        = 65535
}

## Egress rules

resource "aws_network_acl_rule" "nacls_lb5" {
  network_acl_id = aws_network_acl.nacls_lb.id
  rule_number    = 100
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}


# APP group

## Ingress rules

resource "aws_network_acl_rule" "nacls_app1" {
  network_acl_id = aws_network_acl.nacls_app.id
  rule_number    = 100
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.0.0/16"
  from_port      = 1024
  to_port        = 65535
}

## Egress rules

resource "aws_network_acl_rule" "nacls_app2" {
  network_acl_id = aws_network_acl.nacls_app.id
  rule_number    = 100
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}
