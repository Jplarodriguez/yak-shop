Yak shop assignment following the concepts learned in the Bootcamp.
The project has a CI pipeline that will lint and test the code.

## Terraform
Part of Terraform will be to do the networking (VPCs, security groups)
and all the necessary parts to spin up an ECS cluster and deploy the application


## How to run it

### docker
    In order to run with docker, first an image has to be built
        ```docker build -t yakshop .````

    To run the image:
        ```docker run -p 8001:8001 yakshop```
    

### poetry
    The project can also be run directly with poetry.
    ```pip install poetry```
    ```poetry install```
    ```poetry run src/yakhop/main.py```
