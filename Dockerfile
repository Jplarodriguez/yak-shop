FROM python:3.9-slim

WORKDIR /app

COPY pyproject.toml .
COPY poetry.lock .
RUN pip install poetry
COPY . .
RUN poetry install

# COPY ./files ./files

EXPOSE 8001:8001

CMD ["poetry", "run", "python", "src/yakshop/main.py", "--host", "0.0.0.0"]