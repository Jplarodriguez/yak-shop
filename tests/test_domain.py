import pytest
from yakshop.domain import YakOb, YakSex


# Age in days
yak_test = YakOb(
    name="Betty-1",
    age=4,
    sex=YakSex.Female
    )


def test_yak_age_in_days():
    assert yak_test.age_in_days() == 400


# Update age in days
yak_test = YakOb(name="Betty-1", age=4, sex=YakSex.Female)


def test_yak_updated_age_days():
    yak_test.updated_age_days(10)
    assert yak_test._age == 4.1


# Update age in years
yak_test = YakOb(name="Betty-1", age=4, sex=YakSex.Female)


def test_yak_updated_age_years():
    yak_test.updated_age_years(10)
    assert yak_test._age == pytest.approx(14, 0.1)


# Milk at T
@pytest.mark.parametrize(
    "yakky, T, milk_result",
    [
        (YakOb(name="Betty-1", age=4, sex=YakSex.Female), 13, 491.66),
        (YakOb(name="Betty-2", age=8, sex=YakSex.Male), 13, 0),
        (YakOb(name="Betty-3", age=10, sex=YakSex.Female), 20, 0)
    ]
)
def test_milk_at_t(yakky, T, milk_result):
    assert yakky.milk_at_t(T) == milk_result


# Wool at T
@pytest.mark.parametrize(
    "yakky, T, wool_result",
    [
        (YakOb(name="Betty-1", age=4, sex=YakSex.Female), 13, 2),
        (YakOb(name="Betty-2", age=8, sex=YakSex.Male), 13, 1),
        (YakOb(name="Betty-3", age=10, sex=YakSex.Female), 20, 1)
    ]
)
def test_wool_at_t(yakky, T, wool_result):
    assert yakky.wool_at_t(T) == wool_result
