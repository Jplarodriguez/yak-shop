import pytest
from yakshop.domain import YakOb, YakSex
from yakshop.utils import parseXML


file_reader_herd = [
    YakOb(
        name="Betty-1",
        age=4,
        sex=YakSex.Female,
    ),
    YakOb(
        name="Betty-2",
        age=8,
        sex=YakSex.Female,
    ),
    YakOb(
        name="Betty-3",
        age=9.5,
        sex=YakSex.Female,
    )
]


# Errors while parsing
@pytest.mark.parametrize(
    "file_name,result",
    [
        ("tests/files/herd_missing_keys.xml", ValueError),
        ("tests/files/herd_wrong_values_types.xml", ValueError),
    ]
)
def test_parseXML_exceptions(file_name, result):
    with pytest.raises(result):
        parseXML(file_name)


# No errors while parsing
@pytest.mark.parametrize(
    "file_name,result",
    [
        ("tests/files/herd_no_errors.xml", file_reader_herd)
    ]
)
def test_parseXML(file_name, result):
    assert parseXML(file_name) == result
