from sqlalchemy.orm import Session
from sqlalchemy import func
from sqlalchemy.sql.schema import Column
from .models import Order, Yak
from .schemas import Order as OrderSchema
from domain import YakOb
from typing import Callable, List, Set, Union


def get_herd(db: Session) -> Set[YakOb]:
    """ Gets the herd stored in the db

    Args:
        db (Session): Current session of the used database

    Returns:
        Set[YakOb]: Set of YakOb which contains: names, sex and age of the herd
    """
    return ((YakOb(h.name, h.age, h.sex) for h in db.query(Yak).all()))


def get_all_orders(db: Session) -> List[Order]:
    """ Returns all the stored orders in the database

    Args:
        db (Session): Current session of the used database

    Returns:
        [List[Order]]: List with all the stored orders in the db
    """
    return db.query(Order).all()


def _get_stock(
        db: Session,
        column: Column[Union[int, float]],
        time_in_days: int
        ) -> Union[int, float]:
    """ Get the specific stock of a milk/wool in the db

    Args:
        db (Session): Current db session
        column_name (str): milk/wool
        time_in_days (int): Time in days to check the stock from

    Returns:
        Union[int, float]: Total amount of stock of milk/wool
    """
    return db.query(Order)\
        .filter(Order.creation_day <= time_in_days)\
        .with_entities(func.sum(column))\
        .scalar()


def get_milk_stock(db: Session, time_in_days: int) -> Callable:
    """ Get the specific stock of a milk in the db

    Args:
        db (Session): Current db session
        time_in_days (int): Time in days to check the stock from

    Returns:
        Callable: Callback to the _get_stock function
    """
    return _get_stock(db=db, column=Order.milk, time_in_days=time_in_days)


def get_wool_stock(db: Session, time_in_days: int) -> Callable:
    """ Get the specific stock of a wool in the db

    Args:
        db (Session): Current db session
        time_in_days (int): Time in days to check the stock from

    Returns:
        Callable: Callback to the _get_stock function
    """
    return _get_stock(db=db, column=Order.wool, time_in_days=time_in_days)


def create_order(db: Session, order: OrderSchema) -> Order:
    """ Add a new order to the db

    Args:
        db (Session): Current db session
        order (OrderSchema): New order that will be stored

    Returns:
        Order: Stored order
    """
    db_order = Order(
        user_name=order.user_name,
        milk=order.milk,
        wool=order.wool,
        creation_day=order.creation_day
    )

    db.add(db_order)
    db.commit()
    db.refresh(db_order)

    return db_order
