from sqlalchemy import Integer, String, Float, Column, Enum
from yakshop.domain import YakSex
from .db import Base


class Yak(Base):
    __tablename__ = "yak"

    yak_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    age = Column(Float)
    sex = Column(Enum(YakSex))


class Order(Base):
    __tablename__ = "order"

    order_id = Column(Integer, primary_key=True, autoincrement=True)
    user_name = Column(String)
    milk = Column(Float)
    wool = Column(Float)
    creation_day = Column(Integer)
