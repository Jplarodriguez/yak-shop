from .db import Base  # noqa: F401
from .session import engine, SessionLocal
from .models import Yak as YakModel, Order as OrderModel
from utils import parseXML


herdfilepath = "files/herd.xml"


def init_db() -> None:
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)

    init_herd = parseXML(herdfilepath)

    with SessionLocal.begin() as session:
        for yak in init_herd:
            db_yak = YakModel(
                name=yak._name,
                age=yak._age,
                sex=yak._sex
            )
            session.add(db_yak)
        # session.commit()

        # init Orders
        session.add(
            OrderModel(
                user_name="SonGa",
                milk=4.0,
                wool=1,
                creation_day=1
            )
        )
        session.add(
            OrderModel(
                user_name="Leo",
                milk=4.0,
                wool=1,
                creation_day=3
            )
        )
        session.add(
            OrderModel(
                user_name="Emanuel",
                milk=3.2,
                wool=0,
                creation_day=4
            )
        )
        session.commit()
