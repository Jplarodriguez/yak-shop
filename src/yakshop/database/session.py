from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from config import config

engine = create_engine(
        config.settings.SQLALCHEMY_DATABASE_URI,
        pool_pre_ping=True
        )

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
