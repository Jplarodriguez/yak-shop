from yakshop.domain import YakSex
from typing import Optional

from pydantic import BaseModel, PositiveFloat, PositiveInt


class Yak(BaseModel):
    yak_id: Optional[int]
    name: str
    age: PositiveFloat
    sex: YakSex

    class Config:
        orm_mode = True


class Order(BaseModel):
    order_id: Optional[int]
    user_name: str
    milk: PositiveFloat
    wool: PositiveInt
    creation_day: Optional[PositiveInt]

    class Config:
        orm_mode = True
