from fastapi import APIRouter

from .endpoints import herd
from .endpoints import order
from .endpoints import stock

api_router = APIRouter()

api_router.include_router(order.router, tags=["order"])
api_router.include_router(stock.router, tags=["stock"])
api_router.include_router(herd.router, tags=["herd"])
