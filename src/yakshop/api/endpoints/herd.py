from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from database.session import SessionLocal
import database.crud as crud

router = APIRouter()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.get("/yak-shop/herd/{time_in_days}", tags=["herd"])
async def get_herd(time_in_days: int, db: Session = Depends(get_db)):
    # try:
    #     _time_in_days = int(time_in_days)
    # except ValueError:
    #     raise HTTPException(status_code=400, detail="Time is not a number.")

    # Get current herd
    herd = crud.get_herd(db=db)

    if not herd:
        raise HTTPException(status_code=400, detail="Herd does not exist.")

    else:
        updated_herd = []

        for y in herd:
            y.updated_age_days(time_in_days)
            updated_herd.append(
                {"name": y._name, "sex": str(y._sex), "age": y._age}
                )

        return updated_herd
