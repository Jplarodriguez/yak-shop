from typing import List, Dict, Union
from fastapi import APIRouter, Depends, Response
from sqlalchemy.orm import Session, session
from yakshop.database.models import Order

from yakshop.database.session import SessionLocal
import yakshop.database.schemas as schemas
import yakshop.database.crud as crud

from yakshop.order_validation import available_stock


router = APIRouter()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


# ToDo: Do not post younger orders than the oldest already stored order
@router.post("/yak-shop/order/{time_in_days}", tags=["order"])
async def post_order(
        time_in_days: int,
        order: schemas.Order,
        response: Response,
        db: Session = Depends(get_db)
        ) -> Dict[str, Union[Response, int, float]]:
    order.creation_day = time_in_days

    # Get current stock
    available_milk, available_wool = available_stock(db, time_in_days)

    # Response
    response_msg = {}

    # Nothing can be sold
    if order.milk > available_milk and order.wool > available_wool:
        response.status_code = 404
        order.milk = 0
        order.wool = 0

    # Just sell the wool
    elif order.milk > available_milk:
        response.status_code = 206
        order.milk = 0
        response_msg['wool'] = order.wool

    # Just sell the milk
    elif order.wool > available_wool:
        response.status_code = 206
        order.wool = 0
        response_msg['milk'] = order.milk

    else:
        response.status_code = 201
        response_msg['wool'] = order.wool
        response_msg['milk'] = order.milk

    # Store new order
    crud.create_order(db=db, order=order)

    return response_msg


@router.get("/yak-shop/order/", tags=["order"])
async def get_order(db: session = Depends(get_db)) -> List[Order]:
    return crud.get_all_orders(db=db)
