from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from typing import Dict

from database.session import SessionLocal
from order_validation import available_stock

router = APIRouter()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.get("/yak-shop/stock/{time_in_days}", tags=["stock"])
async def get_stock(
        time_in_days: int,
        db: Session = Depends(get_db)
        ) -> Dict[int, float]:
    available_milk, available_wool = available_stock(db, time_in_days)

    if not available_milk or not available_wool:
        raise HTTPException(status_code=400, detail="Stock does not exist.")

    return {"milk": available_milk, "wool": available_wool}
