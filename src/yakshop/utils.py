import xml.etree.ElementTree as ET
from yakshop.domain import YakSex, YakOb
from typing import List


def parseXML(xmlfile: str) -> List[YakOb]:
    """ Parse an xml file with the initial herd information

    Args:
        xmlfile (str): filename path

    Returns:
        List[YakOb]: List of the Yaks in the herd stored in the xml file
    """

    # # check if file exists
    # if not os.path.exists(xmlfile):
    #     raise FileNotFoundError(
    #         errno.ENOENT,
    #         os.strerror(errno.ENOENT),
    #         xmlfile
    #     )
    #     return None

    # create element tree object
    tree = ET.parse(xmlfile)

    # get root element
    root = tree.getroot()

    # iterate yaks
    herditems = [
        YakOb(
                name=str(yak.attrib.get("name")),
                age=float(yak.attrib.get("age")),
                sex=YakSex(yak.attrib.get("sex"))
            )
        for yak in root.findall('labyak')
    ]

    return herditems
