from database.crud import get_herd, get_milk_stock, get_wool_stock
from sqlalchemy.orm import Session
from typing import Union


def available_stock(db: Session, time_in_days: int) -> Union[int, float]:
    """ Calculate the amount of available stock for a specific day.

    Args:
        db (Session): Current db session
        time_in_days (int): Time in days to check the available stock from

    Returns:
        Union[int, float]: Quantity of available milk and wool
    """
    total_milk = 0.0
    total_wool = 0

    for yak in get_herd(db):
        total_milk += yak.milk_at_t(time_in_days)
        total_wool += yak.wool_at_t(time_in_days)

    # Get current stock
    total_ordered_milk = get_milk_stock(db, time_in_days)
    total_ordered_wool = get_wool_stock(db, time_in_days)

    available_milk = total_milk - total_ordered_milk
    available_wool = total_wool - total_ordered_wool

    return available_milk, available_wool
