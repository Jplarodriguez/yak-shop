from pydantic import BaseSettings


class Settings(BaseSettings):
    SQLALCHEMY_DATABASE_URI = "sqlite:///./sql_app.db?check_same_thread=False"


settings = Settings()
