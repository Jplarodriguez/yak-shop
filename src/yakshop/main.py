from fastapi import FastAPI

from api.api import api_router
from database.init_db import init_db

app = FastAPI()

app.include_router(api_router)


@app.get("/")
async def root():
    return {"message": "Hello World"}

if __name__ == "__main__":

    import uvicorn

    init_db()

    uvicorn.run(app, host="0.0.0.0", port=8001, log_level="debug")
