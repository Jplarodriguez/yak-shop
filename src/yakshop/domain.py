from dataclasses import dataclass
from enum import Enum


class YakSex(Enum):
    Female = 'f'
    Male = 'm'


@dataclass(init=False)
class YakOb:
    _name: str
    _age: float  # age in years (1 year is 100days for a Yak)
    _sex: YakSex

    def __init__(self, name: str, age: float, sex: YakSex) -> None:
        self._name = name
        self._age = age
        self._sex = sex

    @property
    def name(self) -> str:
        return self._name

    name.setter

    def name(self, v: str) -> None:
        assert isinstance(v, str)
        self._name = v

    @property
    def age(self) -> float:
        return self._age

    age.setter

    def age(self, v: float) -> None:
        assert isinstance(v, float)
        assert v >= 0
        self._age = v

    @property
    def sex(self) -> YakSex:
        return self._sex
    sex.setter

    def sex(self, v: YakSex) -> None:
        assert isinstance(v, YakSex)
        self._sex = v

    # Methods

    def age_in_days(self) -> int:
        return int(self._age * 100)

    def updated_age_days(self, days: int) -> None:
        self._age += float(days * 0.01)

    def updated_age_years(self, years: float) -> None:
        self._age += years

    def milk_at_t(self, days: int) -> float:
        total_milk = 0
        if self._sex == YakSex.Male or self._age >= 10:
            return total_milk

        # Can be milked
        for day in range(days):
            if self.age_in_days() + day >= 1000:
                break
            total_milk += 50 - (self.age_in_days() + day) * 0.03

        return total_milk

    def wool_at_t(self, days: int) -> int:
        total_wool = 1
        next_shaving_day = 0

        for day in range(days):
            # Check when is next day that can be shaved
            next_shaving_day += int(8 + self._age + float(day * 0.01)) + 1

            #  Is dead or cannot be shaved before the days range?!
            if self._age + float(day * 0.01) >= 10 or next_shaving_day > days:
                break

            else:
                # Add wool to the counter
                total_wool += 1

        return total_wool
